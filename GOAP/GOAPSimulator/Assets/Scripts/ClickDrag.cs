﻿using UnityEngine;
using System.Collections;

public class ClickDrag : MonoBehaviour
{

    private Vector3 offset;

    private RaycastHit hit;

    private bool dragOn = false;

    void Update()
    {
        if( Input.GetMouseButtonDown( 0 ) )
        {
            Vector2 screenPoint = Camera.main.ScreenToWorldPoint( Input.mousePosition );

            if( GetComponent<BoxCollider2D>().OverlapPoint( screenPoint ) )
            {
                offset = Camera.main.ScreenToWorldPoint( Input.mousePosition ) - transform.position;

                dragOn = true;
            }
        }
        if( Input.GetMouseButton( 0 ) )
        {
            if( dragOn )
            {
                transform.position = Camera.main.ScreenToWorldPoint( Input.mousePosition ) - offset;
            }
        }
        if( Input.GetMouseButtonUp( 0 ) )
        {
            dragOn = false;
        }
    }
}
