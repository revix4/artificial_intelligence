﻿using UnityEngine;
using System.Collections;

public class Villager : MonoBehaviour
{

    protected int stamina;
    protected int hunger;
    protected int happiness;

    protected bool hasTool;
    protected int toolDurability;

    protected int wheat;

    //skills
    protected int cookingSkill;
    protected int smithingSkill;
    protected int farmingSkill;

	// Use this for initialization
	void Start ()
	{
	    stamina = 100;
	    hunger = 100;
	    happiness = 100;

	    hasTool = true;
	    toolDurability = 10;

	    wheat = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PickupTool()
    {
        hasTool = true;
        toolDurability = 10;
    }

    public bool farmWheat()
    {
        if (!hasTool)
        {
            return false;
        }
        //stamina--;
        hunger--;
        happiness--;
        toolDurability--;
        if (toolDurability <= 0)
        {
            hasTool = false;
        }
        return true;
    }

    public bool makeCake()
    {
        if (!hasTool || wheat <= 0)
        {
            return false;
        }
        wheat--;
        toolDurability--;

        //stamina--;
        hunger--;
        happiness--;
        if (toolDurability <= 0)
        {
            hasTool = false;
        }
        return true;
    }

    public void takeWheat()
    {
        wheat++;
    }

    public void eatCake()
    {
        hunger += 2;
        happiness++;
    }

    public int getStamina()
    {
        return stamina;
    }
    public int getHunger()
    {
        return hunger;
    }
    public int getHappiness()
    {
        return happiness;
    }

    public bool isTooled()
    {
        return hasTool;
    }

    public int getWheat()
    {
        return wheat;
    }
}
