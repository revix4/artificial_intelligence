﻿using UnityEngine;
using System.Collections;

public class Blacksmith : Villager
{

	// Use this for initialization
	void Start ()
    {
        stamina = 100;
        hunger = 100;
        happiness = 100;

        cookingSkill = 1;
        smithingSkill = 15;
        farmingSkill = 1;

        hasTool = true;
        toolDurability = 10;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
