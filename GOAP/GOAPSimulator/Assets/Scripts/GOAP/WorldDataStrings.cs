﻿using UnityEngine;
using System.Collections;

public class WorldDataStrings
{
    public static string HasCake = "hasCake";
    public static string HasWheat = "hasWheat";
    public static string IHaveTool = "iHaveTool";
    public static string IHaveWheat = "iHaveWheat";
    public static string IAmHappy = "iAmHappy";
}
