﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoapAction
{
    //effects and preconditions
    private Dictionary<string, bool> preconditions;
    private Dictionary<string, bool> effects;

    private bool inRange = false;

    private int cost;

    private GameObject target;

	// Use this for initialization
	protected GoapAction(int cost)
    {
	    target = null;
        preconditions = new Dictionary<string, bool>();
        effects = new Dictionary<string, bool>();

	    this.cost = cost;
    }

    public virtual void reset()
    {
        inRange = false;
        //target = null;
    }

    public bool checkEffects( Dictionary<string, bool> goal )
    {
        foreach( KeyValuePair<string, bool> keyValuePair in goal )
        {
            bool output;
            if( !effects.TryGetValue( keyValuePair.Key, out output ) )
            {
                return false;
            }
            else
            {
                if( output != keyValuePair.Value )
                {
                    return false;
                }
            }
        }
        return true;
    }

    public virtual bool checkPrecondition(Dictionary<string, bool> worldState)
    {
        foreach( KeyValuePair<string, bool> precon in preconditions )
        {
            bool output;
            if( !worldState.TryGetValue(precon.Key, out output) )
            {
                return false;
            }
            else
            {
                if (output != precon.Value)
                {
                    return false;
                }
            }
        }
        return true;
    }

    public virtual void doAction( GameObject self )
    {
        
    }

    public bool isInRange()
    {
        return inRange;
    }

    public void setInRange( bool range )
    {
        inRange = range;
    }

    public int getCost()
    {
        return cost;
    }

    public void addPrecondition( string name, bool requirement )
    {
        preconditions.Add( name, requirement );
    }

    public Dictionary<string, bool> getPreconditions()
    {
        return preconditions;
    }

    public void addEffect(string name, bool result)
    {
        effects.Add(name, result);
    }

    public Dictionary<string, bool> getEffects()
    {
        return effects;
    }

    public GameObject getTarget()
    {
        return target;
    }

    public void setTarget( GameObject targ )
    {
        target = targ;
    }
}
