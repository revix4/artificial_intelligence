﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoapPlanner
{
    private Stack<GoapAction> currentPlan;
    private int recursLevel;

    public Queue<GoapAction> plan( GameObject obj, List<GoapAction> availableActions,
                                   Dictionary<string, bool> worldState, Dictionary<string, bool> goal )
    {
        recursLevel = 0;

        Queue<GoapAction> actionQueue = new Queue<GoapAction>();
        currentPlan = new Stack<GoapAction>();

        recursivePlanning( obj, availableActions, worldState, goal );

        while( currentPlan.Count > 0 )
        {
            actionQueue.Enqueue( currentPlan.Pop() );
        }

        foreach( GoapAction goapAction in actionQueue )
        {
            Debug.Log( goapAction );
        }

        return actionQueue;
    }

    private bool recursivePlanning(GameObject obj, List<GoapAction> remainingActions,
                                   Dictionary<string, bool> worldState, Dictionary<string, bool> goal)
    {
        List<GoapAction> availableActions = new List<GoapAction>(remainingActions);
        recursLevel++;

        //foreach( KeyValuePair<string, bool> keyValuePair in worldState )
        //{
        //    Debug.Log( "World state " + keyValuePair + " " + recursLevel);
        //}

        //reset actions
        foreach ( GoapAction action in availableActions )
        {
            action.reset();
        }

        //remove parts of goal already true


        bool pathFound;
        do
        {
            foreach (KeyValuePair<string, bool> keyValuePair in goal)
            {
                Debug.Log("Goal " + keyValuePair + " " + recursLevel);
            }
            //finding lowest cost action that fulfills the goal
            int lowestCost = int.MaxValue;
            GoapAction lowestAction = null;
            foreach( GoapAction action in availableActions )
            {
                if( action.getCost() < lowestCost && action.checkEffects( goal ) )
                {
                    lowestCost = action.getCost();
                    lowestAction = action;
                }
            }
            //if one could not be found return false
            if( lowestAction == null )
            {
                Debug.Log( "Nothing fulfills goal " + recursLevel );
                recursLevel--;
                return false;
            }
            //otherwise remove from availables
            else
            {
                Debug.Log( "Want do " + lowestAction + " " + recursLevel );
                availableActions.Remove( lowestAction );
            }
            //add to plan
            currentPlan.Push( lowestAction );

            if( lowestAction.checkPrecondition( worldState ) )
            {
                Debug.Log( "Goal is fulfilled" );
                recursLevel--;
                return true;
            }

            pathFound = recursivePlanning( obj, availableActions, worldState, lowestAction.getPreconditions() );

            if( !pathFound )
            {
                currentPlan.Pop();
            }

        } while( !pathFound );
        recursLevel--;
        return true;
    }
}
