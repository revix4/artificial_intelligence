﻿using UnityEngine;
using System.Collections;

public class PickUpWheat : GoapAction {

    public PickUpWheat( int cost ) :
        base( cost )
    {
        addPrecondition(WorldDataStrings.HasWheat, true );
        addEffect(WorldDataStrings.IHaveWheat, true );
    }

    public override void doAction( GameObject self )
    {
        self.GetComponent<Villager>().takeWheat();
        getTarget().GetComponent<Shop>().takeInventory();
    }
}
