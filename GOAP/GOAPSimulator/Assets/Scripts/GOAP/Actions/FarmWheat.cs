﻿using UnityEngine;
using System.Collections;

public class FarmWheat : GoapAction {

	 public FarmWheat(int cost):
        base(cost)
    {
        addPrecondition(WorldDataStrings.IHaveTool, true );
        addEffect(WorldDataStrings.HasWheat, true);
    }

    public override void doAction( GameObject self )
    {
        self.GetComponent<Villager>().farmWheat();
        getTarget().GetComponent<Shop>().addInventory();
    }
}
