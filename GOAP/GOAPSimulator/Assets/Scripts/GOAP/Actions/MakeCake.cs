﻿using UnityEngine;
using System.Collections;

public class MakeCake : GoapAction {

    public MakeCake( int cost ) :
        base( cost )
    {
        addPrecondition(WorldDataStrings.IHaveWheat, true );
        addEffect(WorldDataStrings.HasCake, true);
    }

    public override void doAction( GameObject self )
    {
        self.GetComponent<Villager>().makeCake();
        getTarget().GetComponent<Shop>().addInventory();
    }
}
