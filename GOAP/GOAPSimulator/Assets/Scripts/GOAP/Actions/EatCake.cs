﻿using UnityEngine;
using System.Collections;

public class EatCake : GoapAction
{

    public EatCake(int cost) :
        base( cost )
    {
        addPrecondition(WorldDataStrings.HasCake, true);
        addEffect(WorldDataStrings.IAmHappy, true);
    }

    public override void doAction(GameObject self)
    {
        self.GetComponent<Villager>().eatCake();
        getTarget().GetComponent<Shop>().takeInventory();
    }
}
