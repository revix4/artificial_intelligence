﻿using UnityEngine;
using System.Collections;

public class PickUpTool : GoapAction {

    public PickUpTool( int cost ) :
        base( cost )
    {
        addPrecondition(WorldDataStrings.IHaveTool, false );
        addEffect(WorldDataStrings.IHaveTool, true );
    }

    public override void doAction( GameObject self )
    {
        self.GetComponent<Villager>().PickupTool();
    }
}
