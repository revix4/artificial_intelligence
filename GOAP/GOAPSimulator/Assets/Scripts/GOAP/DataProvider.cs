﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataProvider
{
    private Shop[] allShops;


    public DataProvider()
    {
        allShops = Object.FindObjectsOfType( typeof( Shop ) ) as Shop[];
    }

    public Dictionary<string, bool> getWorldState(Villager me)
    {
        Dictionary<string, bool> worldState = new Dictionary<string, bool>();

        foreach( Shop shop in allShops )
        {
            if( shop is CakeShop )
            {
                worldState.Add( WorldDataStrings.HasCake, shop.getInventory() > 0 );
            }
            else if( shop is WheatFarm )
            {
                worldState.Add(WorldDataStrings.HasWheat, shop.getInventory() > 0);
            }
        }

        worldState.Add(WorldDataStrings.IHaveTool, me.isTooled() );
        worldState.Add(WorldDataStrings.IHaveWheat, me.getWheat() > 0 );
        worldState.Add(WorldDataStrings.IAmHappy, me.getHappiness() > 50 );

        return worldState;
    }
}
