﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GoapAgent : MonoBehaviour
{
    private List<GoapAction> availableActions;
    private Queue<GoapAction> actionQueue;

    private FSM stateMachine;

    private GoapPlanner planner;
    private bool hasPlan;

    private DataProvider worldProvider;

	// Use this for initialization
	void Start ()
    {
	    availableActions = new List<GoapAction>();
        actionQueue = new Queue<GoapAction>();
	    stateMachine = gameObject.GetComponent<FSM>();

        planner = new GoapPlanner();
	    hasPlan = false;

        worldProvider = new DataProvider();

        setStates();
        setActions();
    }

    private void setStates()
    {
        State idleState = new Idle( gameObject );
        State moveState = new MoveTo( gameObject );
        State actionState = new PerformAction( gameObject );

        idleState.addTransition( new HasPlan( moveState.getStateName() ) );

        moveState.addTransition( new ArrivedAtTarget( actionState.getStateName() ) );

        actionState.addTransition( new NoMoreAction( idleState.getStateName() ) );
        actionState.addTransition( new NeedToMove( moveState.getStateName() ) );

        stateMachine.addStateToList( idleState );
        stateMachine.addStateToList( moveState );
        stateMachine.addStateToList( actionState );

        stateMachine.setStartState( idleState.getStateName() );
    }

    private void setActions()
    {
        GoapAction eatCake = new EatCake( 1 );
        eatCake.setTarget( GameObject.FindWithTag( "CakeShop" ) );

        GoapAction farmWheat = new FarmWheat(10);
        farmWheat.setTarget( GameObject.FindWithTag( "WheatFarm" ) );

        GoapAction makeCake = new MakeCake( 3 );
        makeCake.setTarget(GameObject.FindWithTag("CakeShop"));

        GoapAction pickUpTool = new PickUpTool( 2 );
        pickUpTool.setTarget(GameObject.FindWithTag( "Smithy" ));

        GoapAction pickUpWheat = new PickUpWheat( 5 );
        pickUpWheat.setTarget(GameObject.FindWithTag("WheatFarm"));

        availableActions.Add(eatCake);
        availableActions.Add(farmWheat);
        availableActions.Add(makeCake);
        availableActions.Add(pickUpTool);
        availableActions.Add(pickUpWheat);
    }

    public bool makePlan(Dictionary<string, bool> goal)
    {
        if( !hasPlan )
        {
            //make plan
            actionQueue = planner.plan( gameObject, availableActions, worldProvider.getWorldState( GetComponent<Villager>() ), goal );
            if( actionQueue.Count > 0 )
            {
                hasPlan = true;
            }
        }
        return hasPlan;
    }

    public GameObject getCurrentTarget()
    {
        return actionQueue.Peek().getTarget();
    }

    public int getActionCount()
    {
        return actionQueue.Count;
    }
	
	// Update is called once per frame
	public void Update ()
    {
	    if( actionQueue.Count == 0 )
	    {
	        hasPlan = false;
	    }
	}

    public void doNextAction()
    {
        GoapAction actionToDo = actionQueue.Dequeue();
        actionToDo.doAction( gameObject );
    }
}
