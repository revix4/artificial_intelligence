﻿using UnityEngine;
using System.Collections;

public class Farmer : Villager
{
	// Use this for initialization
	void Start ()
    {
        stamina = 100;
        hunger = 100;
        happiness = 100;

        cookingSkill = 3;
        smithingSkill = 5;
        farmingSkill = 10;

        hasTool = true;
        toolDurability = 10;
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}
}
