﻿using UnityEngine;
using System.Collections;

public class Chef : Villager
{
	// Use this for initialization
	void Start ()
    {
        stamina = 100;
        hunger = 100;
        happiness = 100;

        cookingSkill = 10;
        smithingSkill = 1;
        farmingSkill = 5;

        hasTool = false;
        toolDurability = 0;

        wheat = 0;
    }
}
