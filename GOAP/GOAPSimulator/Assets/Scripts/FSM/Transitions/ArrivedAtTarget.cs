﻿using UnityEngine;
using System.Collections;

public class ArrivedAtTarget : Transition {

    public ArrivedAtTarget( string nextStateName ) : base( nextStateName )
    {
        
    }

    public override bool condition( GameObject me )
    {
        if (Vector3.Distance(me.transform.position, me.GetComponent<GoapAgent>().getCurrentTarget().transform.position) <= 0.1f)
        {
            return true;
        }
        return false;
    }
}
