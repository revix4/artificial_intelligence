﻿using UnityEngine;
using System.Collections;

public class Transition {
	protected string nextState;

	public Transition(string nextStateName)
	{
		nextState = nextStateName;
	}

	public virtual bool condition(GameObject me)
	{
		return false;
	}

	public virtual void startTransition()
	{

	}

	public virtual void endTransition()
	{

	}

	public string getNextState()
	{
		return nextState;
	}
}
