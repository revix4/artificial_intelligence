﻿using UnityEngine;
using System.Collections;

public class NoMoreAction : Transition
{

    public NoMoreAction(string nextStateName) : base( nextStateName )
    {

    }

    public override bool condition(GameObject me)
    {
        if( me.GetComponent<GoapAgent>().getActionCount() <= 0)
        {
            return true;
        }
        return false;
    }
}
