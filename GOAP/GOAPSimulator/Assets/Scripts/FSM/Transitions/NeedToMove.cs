﻿using UnityEngine;
using System.Collections;

public class NeedToMove : Transition
{

    public NeedToMove(string nextStateName) : base( nextStateName )
    {

    }

    public override bool condition(GameObject me)
    {
        if( Vector3.Distance( me.transform.position, me.GetComponent<GoapAgent>().getCurrentTarget().transform.position ) > 1.0f )
        {
            return true;
        }
        return false;
    }
}

