﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Idle : State
{

    public Idle(GameObject myself):base(myself)
    {
        StateName = "Idle";
    }

    public override void update()
    {
        Dictionary<string, bool> goal = new Dictionary<string, bool>();
        goal.Add( WorldDataStrings.HasCake, true );
        //find next goal and make a plan
        if ( !me.GetComponent<GoapAgent>().makePlan(goal) )
        {
            Debug.Log("ERROR UNABLE TO FORM PLAN");
        }
        else
        {
            Debug.Log("PLAN WAS MADE");
        }
    }
}
