﻿using UnityEngine;
using System.Collections;

public class MoveTo : State
{
    private Vector3 startPos;

    private float lerpTime;
    private float elapsedTime;

    public MoveTo( GameObject myself ) : base( myself )
    {
        StateName = "MoveTo";

        lerpTime = 1.0f;
        elapsedTime = 0.0f;
    }

    public override void update()
    {
        Debug.Log( "MoveTo" );

        //move to where we need to be to preform the action
        Vector3 targetPos = me.GetComponent<GoapAgent>().getCurrentTarget().transform.position;

        me.transform.position = Vector3.Lerp( startPos, targetPos, elapsedTime/lerpTime );
        elapsedTime += Time.deltaTime;
    }

    public override void enterState()
    {
        startPos = me.transform.position;
        elapsedTime = 0.0f;
    }
}
