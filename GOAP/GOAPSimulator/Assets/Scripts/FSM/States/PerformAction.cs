﻿using UnityEngine;
using System.Collections;

public class PerformAction : State {

    public PerformAction( GameObject myself ) : base( myself )
    {
        StateName = "PerformAction";
    }

    public override void update()
    {
        Debug.Log( "PerformAction" );
        me.GetComponent<GoapAgent>().doNextAction();
    }
}
