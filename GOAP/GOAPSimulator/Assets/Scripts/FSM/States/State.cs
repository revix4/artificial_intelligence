﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class State {

	protected string StateName;
	protected List<Transition> transitions;
	protected GameObject me;

	public State(GameObject myself)
	{
		StateName = "BaseState";
		transitions = new List<Transition> ();
		me = myself;
	}

	public virtual string checkTransitions(GameObject me)
	{
		foreach (Transition transition in transitions) 
		{
			if(transition.condition(me))
			{
				transition.startTransition();
				transition.endTransition();
				return transition.getNextState();
			}
		}
		return StateName;
	}

	public void addTransition(Transition trans)
	{
		transitions.Add (trans);
	}

	public virtual void update()
	{
		Debug.Log ("Base");
	}

	public virtual void enterState()
	{

	}

	public virtual void exitState()
	{

	}

	public string getStateName()
	{
		return StateName;
	}
}
