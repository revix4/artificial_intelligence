﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FSM : MonoBehaviour {
	State currentState;
	Dictionary<string, State> stateList = new Dictionary<string, State> ();
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (currentState != null)
		{
			string prevState = currentState.getStateName ();
			string nextState;

			currentState.update ();

			nextState = currentState.checkTransitions (this.gameObject);

			if (prevState != nextState) 
			{
				currentState.exitState ();
				currentState = stateList [nextState];
				currentState.enterState ();
			}
		}
	}

	public void addStateToList(State state)
	{
		stateList.Add (state.getStateName (), state);
	}

	public void setStartState(string stateName)
	{
		currentState = stateList[stateName];
	}
    public string getCurState()
    {
       return currentState.getStateName();
    }
}
