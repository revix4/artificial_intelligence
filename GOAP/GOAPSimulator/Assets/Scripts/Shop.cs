﻿using UnityEngine;
using System.Collections;

public class Shop : MonoBehaviour
{

    protected int itemsInInventory;


    virtual public void consume()
    {
        
    }

    public void addInventory()
    {
        itemsInInventory++;
    }

    public bool takeInventory()
    {
        if( itemsInInventory == 0 )
        {
            return false;
        }
        itemsInInventory--;
        return true;
    }

    public int getInventory()
    {
        return itemsInInventory;
    }
}
